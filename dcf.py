#!/usr/bin/env python3
import math
import numpy as np

fcf = []
fcf_sum = 0
fcf_avg = 0
fcf_grow = []
fcf_grow_sum = 0
fcf_grow_avg = 0

fcf = input("FCF: ")
fcf = fcf.replace(',', '')
fcf = [int(e) if e.isdigit() else e for e in fcf.split()]

for i in range(len(fcf)):
    fcf_sum = fcf_sum + fcf[i]

for i in range(len(fcf) -1):
    fcf_grow.append((fcf[i + 1] - fcf[i]) / fcf[i])

for i in range(len(fcf_grow)):
    fcf_grow_sum = fcf_grow_sum + fcf_grow[i]

fcf_avg = fcf_sum / len(fcf)
fcf_grow_avg = math.floor((fcf_grow_sum / len(fcf_grow)) * 100)

fcf_pre = fcf_avg
fcf_pre_ls = []
for i in range(1, 11):
    fcf_pre = fcf_pre * (1 + (fcf_grow_avg / 100))
    fcf_pre_ls.append(math.floor(fcf_pre))


print("The FCF average is: ", fcf_avg)
print(fcf_grow)
print("The FCF growth average is {} %".format(fcf_grow_avg))
print(fcf_pre)
print(fcf_pre_ls)
