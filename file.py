#!/usr/bin/env python3
import os

with open("mydata.txt", mode="w", encoding="utf-8") as my_file:
     my_file.write("Some random text \nMore random text\nAnd some more")

with open("mydata.txt", encoding="utf-8") as my_file:
    print(my_file.read())

print(my_file.closed)
print(my_file.name)
print(my_file.mode)

os.rename("mydata.txt", "mydata2.txt")
with open("mydata2.txt", encoding="utf-8") as my_file:
    line_num = 1
    while True:
        line = my_file.readline()
        if not line:
            break
        print("Line: ", line_num, " : ", line, end="")
        line_num += 1

# os.rename("mydata.txt", "mydata2.txt")
# os.remove("mydata2.txt")
# os.mkdir("mydir")
# os.chdir("mydir")
# print("PWD", os.getcwd())
# os.chdir("..")
# os.rmdir("mydir")
