#!/usr/bin/env python3
# > : Greater than
# < : Less than
# >= : Greater than or equal to
# <= : Less than or equal to
# == : Equal to
# != : Not equal to
#
drink = input("Pick one (Coke or Pepsi) : ")

if drink == "Coke":
    print("Here is your Coke")
elif drink == "Pepsi":
    print("Here is your Pepsi")
else:
    print("Here is your Water")

# and : Both are true
# or : If one is true
# not : Converts true into false

age = int(input("Enter Age: "))

if (age >= 1) and (age <= 18):
    print("Important Birthday")
elif (age == 21) or (age == 50):
    print("Important Birthday")
elif not age < 65:
    print("Important Birthday")
else:
    print("Not Important")
