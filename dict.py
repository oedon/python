#!/usr/bin/env python3
fdict = {"f_name": "Chris", "l_name": "Kraft", "addr": "123 Main St"}

print("My Name is", fdict["f_name"])

fdict["addr"] = "215 North St"
fdict["city"] = "Pittsburgh"
print("What city:", "city" in fdict)
print(fdict.values())
print(fdict.keys())
print(fdict.items())

for k, v in fdict.items():
    print(k, v)

print(fdict.get("m_name", "nope"))
print(fdict.get("f_name", "nope"))

del fdict["f_name"]
for i in fdict:
    print(i)
fdict.clear()
print("-----------------")

employees = []
f_name, l_name = input("Enter Name: ").split()
employees.append({"f_name": f_name, "l_name": l_name})
