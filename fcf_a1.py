#!/usr/bin/env python3
import math
import numpy as np


class Flow:
    def __init__(self, fcf):
        self.fcf = fcf

    def get_some(self):
        self.fcf = self.fcf.replace(",", "")
        # Fix negative numbers
        self.fcf = [int(e) if e.isdigit() else e for e in self.fcf.split()]
        return self.fcf

    def fcf_avg(self):
        self.fcf_sum = 0
        self.fcf_avg = 0
        for i in range(len(self.fcf)):
            self.fcf_sum = self.fcf_sum + self.fcf[i]
        self.fcf_avg = math.floor(self.fcf_sum / len(self.fcf))
        print("The FCF average is:", self.fcf_avg)
        return self.fcf_avg

    def fcf_grow(self):
        self.fcf_grow = []
        self.fcf_grow_sum = 0
        self.fcf_grow_avg = 0
        for i in range(len(self.fcf) - 1):
            self.fcf_grow.append((self.fcf[i + 1] - self.fcf[i]) / self.fcf[i])
        for i in range(len(self.fcf_grow)):
            self.fcf_grow_sum = self.fcf_grow_sum + self.fcf_grow[i]
        self.fcf_grow_avg = math.floor((self.fcf_grow_sum / len(self.fcf_grow)) * 100)
        print("The FCF growth average is {} %".format(self.fcf_grow_avg))
        return self.fcf_grow

    def fcf_pre(self):
        self.fcf_pre = self.fcf_avg
        self.fcf_pre_ls = []
        for i in range(1, 11):
            self.fcf_pre = self.fcf_pre * (1 + (self.fcf_grow_avg / 100))
            self.fcf_pre_ls.append(math.floor(self.fcf_pre))
        print(
            "FCF in Year {} is expected to be {}".format(
                len(self.fcf), self.fcf_pre_ls[-1]
            )
        )


def main():
    flow = Flow(fcf=input("Input FCF: "))
    flow.get_some()
    flow.fcf_avg()
    flow.fcf_grow()
    flow.fcf_pre()


main()
