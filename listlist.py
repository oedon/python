#!/usr/bin/env python3
import math

num = [1, 2, 3, 4, 5]
value = [[math.pow(m, 2), math.pow(m, 3), math.pow(m, 4)] for m in num]

print(value)

mult_val = [[0] * 10 for i in range(10)]
mult = []

for i in range(10):
    for j in range(10):
        mult_val[i][j] = i, j

for i in range(10):
    for j in range(10):
        print(mult_val[i][j], end='')
    print()

for i in range(1, 10):
    for j in range(1, 10):
        print(i * j, end=', ')
    print()
