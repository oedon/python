#!/usr/bin/env python3
import math

for i in range(1, 10):
    for k in range(1, 10):
        print(i * k, end=", ")
    print()

mult_table = [[0] * 10 for i in range(10)]
print(mult_table)
for i in range(1, 10):
    for j in range(1, 10):
        mult_table[i][j] = i * j


for i in range(1, 10):
    for j in range(1, 10):
        print(mult_table[i][j], end=", ")
    print()
