import random

while True:
    user_input = input("Type Rock/Paper/Scissors or Q to quit.: ")
    user_input = user_input.upper()
    if user_input[0] == "Q":
        break
    ls = ["ROCK", "PAPER", "SCISSORS"]
    rand_num = random.randint(0, 2)
    cpu_pick = ls[rand_num]
    print(f"Computer chooses: {cpu_pick}")
    if (cpu_pick == user_input):
        print("TIE")
        continue
    while cpu_pick != user_input:
        if cpu_pick == "ROCK" and user_input == "PAPER":
            print("YOU WIN")
            break
        if cpu_pick == "ROCK" and user_input == "SCISSORS":
            print("YOU LOSE")
            break
        if cpu_pick == "PAPER" and user_input == "ROCK":
            print("YOU LOSE")
            break
        if cpu_pick == "PAPER" and user_input == "SCISSORS":
            print("YOU WIN")
            break
        if cpu_pick == "SCISSORS" and user_input == "ROCK":
            print("YOU WIN")
            break
        if cpu_pick == "SCISSORS" and user_input == "Paper":
            print("YOU LOSE")
            break
        else:
            print("Typo?")
            break
        
