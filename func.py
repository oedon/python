#!/usr/bin/env python3
def add_numbers(num1, num2):
    return num1 + num2

print("5 + 4 =", add_numbers(5, 4))

def change_name():
    global gbl_name
    gbl_name = "Sammy"

gbl_name = "Sally"
print(gbl_name)
change_name()
print(gbl_name)
