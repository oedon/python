#!/usr/bin/env python3
def mult_divide(num_1, num_2):
    return(num_1 * num_2), (num_1 / num_2)

mult, divide = mult_divide(5, 4)

print(mult)
print(divide)
