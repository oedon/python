#!/usr/bin/env python3
def factorial(num):
    if num <= 1:
        return 1
    else:
        result = num * factorial(num - 1)
        return result


print(factorial(4))


def fibo(num1):
    if num1 == 1:
        return 1
    elif num1 == 0:
        return 0
    else:
        result = fibo(num1 - 1) + fibo(num1 - 2)
        return result


print(fibo(5))


fib = []

x = int(input("How many Fibonacci values?"))


def fibo(x):
    if x == 1:
        return 1
    elif x == 0:
        return 0
    else:
        result = fibo(x - 1) + fibo(x - 2)
        return result


for i in range(x):
    fib.append(fibo(i))

print(fib)
