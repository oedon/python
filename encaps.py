class SoftwareEngineer:
    def __init__(self, name=None, age=None) -> None:
        self.name = name
        self.age = age
        self._salary = None
        self._bugs = 0


    def code(self):
        self._bugs += 1

    # getter
    def get_salary(self):
        return self._salary
    # setter
    def set_salary(self, base_value):
        # check value, enforce contrains
        # if base_value < 1000:
        #     self._salary = 1000
        # if base_value > 20000:
        #     self._salary = 20000
        self._salary = self._calculate_salary(base_value)

    def _calculate_salary(self, base_value):
        if self._bugs < 10:
            return base_value
        if self._bugs < 100:
            return base_value * 2
        return base_value * 3



se = SoftwareEngineer("Max", 25)
print(se.age, se.name)

for i in range(70):
    se.code()

se.set_salary(6000)
print(se.get_salary())
