#!/ wwusr/bin/env python3
from matplotlib import pyplot as plt

z = [9.664, 9.422, 9.494, 9.736, 9.168, 9.568, 9.216, 9.722, 9.168, 9.444]
x = [0.595, 0.691, 0.635, 0.484, 0.544, 0.576, 0.775, 0.998, 1.192, 0.767]
y = [1.671, 1.827, 1.861, 1.748, 2.590, 2.429, 2.626, 2.432, 2.499, 2.640]
norm = [2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]

time = [0.005, 0.010, 0.015, 0.020, 0.025, 0.030, 0.035, 0.040, 0.045, 0.050]

plt.title("GOPRO")
plt.xlabel("Time in s")
plt.ylabel("m/s²")

plt.plot(time, x, label="x")
plt.plot(time, y, label="y")
plt.plot(time, z, label="z")
plt.plot(time, norm, 'k--', label='Norm')
plt.legend()
plt.show()
