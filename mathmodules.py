#!/usr/bin/env python3
import math

# Round up
print("ceil(4.4) = ", math.ceil(4.4))
# Round down
print("floor(4.4) = ", math.floor(4.4))
# Absolute
print("fabs(-4.4) = ", math.fabs(-4.4))
# Factorial = 1 * 2 * 3
print("factorial(4) = ", math.factorial(4))
# Return remainder of division
print("fmod(5,4) = ", math.fmod(5,4))
# Receive a float and return an int
print("trunc(4.2) = ", math.trunc(4.2))
# Return x^y
print("pow(2,2) = ", math.pow(2,2))
# Return square root
print("sqrt(2) = ", math.sqrt(2))
# Special
print("math.e = ", math.e)
print("math.pi = ", math.pi)

# Log base and 10^3 = 1000
print("log(1000,10) = ", math.log(1000,10))
print("log10(1000) = ", math.log10(1000))
print("ln(10) = ", math.log(10,math.e))

# Trig functions sin, cos, tan, asin, acos, atan, atan2, asinh, acosh, atanh, sinh, cosh, tanh
print("sin(1) = ", math.sin(1))

# Convert radians to degrees
print("degrees(1.5708) = ", math.degrees(1.5708))
print("radians(90) = ", math.radians(90))
