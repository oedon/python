#!/usr/bin/env python3
for i in range(21):
    x = i % 2 == 0
    if x == False:
        print("All odd numbers for 0-20 :", i)

for i in range(1, 21):
    if (i % 2) != 0:
        print("i =", i)
