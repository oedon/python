#!/usr/bin/env python3

size = int(input("How tall is the tree :"))

spaces = size -1
hashes = 1
stump_spaces = size -1

while size != 0:
    for i in range(spaces):
        print(" ", end="")
    for i in range(hashes):
        print("#", end="")
    print()
    spaces -= 1
    hashes += 2
    size -= 1
for i in range(stump_spaces):
    print(" ", end="")
print("#")
