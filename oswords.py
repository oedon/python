#!/usr/bin/env python3
import os

with open("words.txt", mode="w", encoding="utf-8") as my_file:
    my_file.write("Some random text \nMore random text \nAnd some more")

with open("words.txt", encoding="utf-8") as my_file:
    for i in range(3):
        line = my_file.readline()
        strip = line.replace(" ", "").strip()
        print("Line: ", i+1, "Number of Words: ", len(line.split()))
        print("Avg Word Length: {:.1f}".format(len(strip) / len(line.split())))
