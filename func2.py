#!/usr/bin/env python3
def is_float(str_val):
    try:
        float(str_val)
        return True
    except ValueError:
        return False

pi = 3.14
print("Is float?", is_float(pi))
