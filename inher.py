#!/usr/bin/env python3
class Employee:
    def __init__(self, name, age, salary):
        self.name = name
        self.age = age
        self.salary = salary

    def work(self):
        print(f"{self.name} is working...")


class SoftwareEngineer(Employee):
    def __init__(self, name, age, salary, level):
        super().__init__(name, age, salary)  # inherit init from parent
        self.level = level  # extend variables only for this class

    def work(self):
        print(f"{self.name} is coding...")  # Override parent function

    def debug(self):
        print(f"{self.name} is debugging...")


class Designer(Employee):
    def draw(self):
        print(f"{self.name} is drawing...")


se = SoftwareEngineer("Alex", 25, 5000, "Junior")
print(se.name)
se.work()
print(se.level)
se.debug()

d = Designer("Max", 27, 7000)
print(d.name)
d.work()
d.draw()


# Polymorphism
employees = [
    SoftwareEngineer("Alex", 25, 5000, "Junior"),
    SoftwareEngineer("Lisa", 23, 4000, "Junior"),
    Designer("Max", 27, 7000),
]


def motivate(employees):
    for emplyee in employees:
        emplyee.work()


motivate(employees)
