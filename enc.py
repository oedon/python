#!/usr/bin/env python3
dec = input("Input message to encrypt: ")

dec_ls = list(dec)
dec2_ls = []
enc_ls = []

i = 0
while i < len(dec_ls):
    enc_ls.append(ord(dec_ls[i]) + 4 )
    i += 1

l = 0
while l < len(enc_ls):
    dec2_ls.append(enc_ls[l] - 4)
    l += 1

y = 0
while y < len(enc_ls):
    print(chr(enc_ls[y]), end="")
    y += 1

print()

x = 0
while x < len(dec2_ls):
    print(chr(dec2_ls[x]), end="")
    x += 1
