#!/usr/bin/env python3
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

data_file = "clean.txt"

data = pd.read_csv(data_file)

time = np.arange(0, 1.005, 0.005)

print(data.shape)
