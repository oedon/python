#!./.venv/bin/python3
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# plt.style.use('fivethirtyeight')
data_file = "clean.txt"

column_list = ["x", "y", "z"]
data = pd.read_csv(data_file, names=column_list)

x = data['x'].tolist()
y = data['y'].tolist()
z = data['z'].tolist()
time = np.arange(0, 1, 0.005)
# norm = [2] * 200

plt.title("GOPRO")
plt.xlabel("Time in s")
plt.ylabel("m/s²")
# plt.plot(time, norm, 'k--', label='Norm')

plt.plot(time, y, label='y')
plt.plot(time, x, label='x')
plt.plot(time, z, label='z')

plt.grid(True)
plt.legend()
plt.show()
