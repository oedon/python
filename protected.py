class Employees:
    
    def __init__(self, name=None, age=None):
        self.name = name
        self.age = age
        self._salary = None

    @property
    def salary(self):
        return self._salary


    @salary.setter
    def salary(self, value):
        self._salary = value



def main():
    e = Employees("Peter", 25)
    print(id(e._salary))
    print(id(e.salary))
    e.salary = 3000
    print(id(e._salary))

main()
