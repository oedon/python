#!/usr/bin/env python3
import random

rand_num = random.randrange(1,11)

while True:
    try:
        guess = int(input("Guess a number between 1 - 10: "))
        if guess != rand_num:
            print("Try Again!")
            continue
        else:
            print("Got it!")
            break

    except ValueError:
        print("No Number!")

    

print(rand_num)
