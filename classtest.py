class Animal:
    def __init__(self, test=None):
        self.test = test

    def run(self):
        self.test = input("GET SOME: ")
        print("Run Test {}".format(self.test))

    def nope(self):
        print(self.test)


def main():
    animal = Animal()
    animal.run()
    animal.nope()


main()
