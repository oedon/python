#!/usr/bin/env python3
print(type("3"))
print(type('3'))
print(type('''3'''))

samp_string = "Very important string"
print("Length:", len(samp_string))
print(samp_string[0])
print(samp_string[-1])
print(samp_string[0:6])
print(samp_string[8:])
print(samp_string[0:-1:2])
print(samp_string[::-1])

print("Green " + "Eggs")
print("Hello " * 5)
num_string = str(4)

for char in samp_string:
    print(char)

for i in range(0, len(samp_string)-1, 2):
    print(samp_string[i] + samp_string[i+1])

print("A =", ord("A"))
print("65 =", chr(65))
