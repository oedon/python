#!/usr/bin/env python3
import sys

my_age = 43
my_name = "Cole"

print("Hello", my_name)

# Comment

'''
Multiline
Comment
'''

str1 = "String"
str2 = 'String'
str3 = "\"This is a quote\""

print(sys.maxsize)
print(sys.float_info.max)

cn_1 = 5 + 6j
print(cn_1)

can_vote = True

print("Cast", type(int(5.4)))
print("Cast 2", type(str(5.4)))
print("Cast 3", type(chr(99)))
print("Cast 3", type(ord('a')))
print("Cast 3", type(float(2)))

num_1 = "1"
num_2 = "2"
print("1 + 2 =", (int(num_1) + int(num_2)))
