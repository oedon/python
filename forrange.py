#!/usr/bin/env python3
for i in [2, 4, 6, 8, 10]:
    print("i =", i)

for i in range(5):
    print("i =", i)

for i in range(2, 5):
    print("i =", i)

i = 6
print("Is 6 Even :", (i % 2 == 0))

your_float = input("Enter a float: ")
your_float = float(your_float)
print("Rounded to 2 Decimals : {:.2f}".format(your_float))
