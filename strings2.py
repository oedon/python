#!/usr/bin/env python3
rand_string = "      this is an important string"

rand_string = rand_string.lstrip()
rand_string = rand_string.rstrip()
rand_string = rand_string.strip()

print(rand_string)

rnd_string = "test string"
print(rnd_string.capitalize())
print(rnd_string.upper())
print(rnd_string.lower())

a_list = ["Bunch", "of", "Shit"]
print(" ".join(a_list))
print(".".join(a_list))
print(a_list)

b_list = rnd_string.split()

for i in b_list:
    print(i)

print("how many: ", rnd_string.count("test"))
print("where is: ", rnd_string.find("test"))
print("where is: ", rnd_string.replace("test", "TEST"))

letter_z = "z"
print("Is z a letter or number?", letter_z.isalnum)
print("Is z a letter or number?", letter_z.isalpha)
print("Is z a letter or number?", letter_z.isdigit)
print("Is z a letter or number?", letter_z.islower)
print("Is z a letter or number?", letter_z.isupper)
print("Is z a letter or number?", letter_z.isspace)
