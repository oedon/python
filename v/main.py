import streamlit as st
# To make things easier later, we're also importing numpy and pandas for
# working with sample data.
import numpy as np
import pandas as pd

st.title('PPC-AVS')
number = st.slider('v0', 0, 100)
acc = st.slider('a', 0.1, 10.0)

time = 0.0
#dist = 0.0
dec = 0 - acc

thislist = [number]
timelist = [time]
distlist = []

dist = (number/3.6) * time - 0.5 * dec * (time * time)
distlist.append(dist)

while number > 0:
    number = (number/3.6) - acc * 0.1
    thislist.append(number * 3.6)
    number = number * 3.6
    time = time + 0.1
    timelist.append(time)
    dist = (number/3.6) * time - 0.5 * dec * (time * time)
    distlist.append(dist)


chart_data = pd.DataFrame(
        thislist,
        columns=['v(t)'])

chart_data2 = pd.DataFrame(
        distlist,
        columns=['s(t)'])

st.line_chart(chart_data)
st.line_chart(chart_data2)

df = pd.DataFrame({
  'km/h': thislist,
  'time': timelist,
  'm': distlist,
})

df
