#!/usr/bin/env python3
my_tuple = (1, 2, 3, 5, 8)
print(my_tuple[0])
print(my_tuple[0:3])
print(len(my_tuple))

more_fibs = my_tuple + (13, 21, 34)
print(34 in more_fibs)

for i in more_fibs:
    print(i)

a_list = [55, 89, 144]
a_tuple = tuple(a_list)
a_list = list(a_tuple)
print(max(a_tuple))
print(min(a_tuple))
