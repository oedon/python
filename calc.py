#!/usr/bin/env python3
import math

num_1, num_2 = input("Enter 2 Numbers: ").split()
operator = input("Enter Operator (+ - * /): ")

num_1 = int(num_1)
num_2 = int(num_2)

if operator == "+":
    print("{} + {} = {}".format(num_1, num_2, num_1 + num_2))
elif operator == "-":
    print("{} - {} = {}".format(num_1, num_2, num_1 - num_2))
elif operator == "*":
    print("{} * {} = {}".format(num_1, num_2, num_1 * num_2))
elif operator == "/":
    print("{} / {} = {}".format(num_1, num_2, num_1 / num_2))
else:
    print("Wrong Operator!")
