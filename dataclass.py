from dataclasses import dataclass

@dataclass
class Person:
    name: str
    address: str

    @property
    def salary(self):
        return self._salary

    @salary.setter
    def salary(self, value):
        self._salary = value


def main():
    person = Person("John", "123 Main St")
    print(person.name, person.address)
    person.salary = 3000
    print(person.salary)

if __name__ == "__main__":
    main()

