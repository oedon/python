#!/usr/bin/env python3
# name = input("What is your name: ")
# print("Hello", name)
#
# num_1, num_2 = input("Enter 2 Numbers: ").split()
# num_1 = int(num_1)
# num_2 = int(num_2)
# sum_1 = num_1 + num_2
# diff_1 = num_1 - num_2
# product_1 = num_1 * num_2
# quotient_1 = num_1 / num_2
# remain_1 = num_1 % num_2
#
# print("{} + {} = {}".format(num_1, num_2, sum_1))
# print("{} - {} = {}".format(num_1, num_2, diff_1))
# print("{} * {} = {}".format(num_1, num_2, product_1))
# print("{} / {} = {}".format(num_1, num_2, quotient_1))
# print("{} % {} = {}".format(num_1, num_2, remain_1))
#
miles = input("Input miles: ")
miles = int(miles)
km = miles * 1.60934

print("{} miles are {} km".format(miles, km))
