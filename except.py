#!/usr/bin/env python3
while True:
    try:
        number = int(input("Please enter a number: "))
        break

    except ValueError:
        print("No Number!")
    except:
        print("An unkown error occurred")

print("Thanks for the number!")
